require 'nokogiri'
require 'rest-client'
require 'uri'
require 'htmlentities'
require 'json'
require 'fileutils'

SITE_ROOT   = "http://planzajec.uek.krakow.pl/"
TMP_DIR     = Dir.pwd + "/tmp"
UPLOAD_DIR  = TMP_DIR + "/upload"
SLEEP_TIME  = 0.1 

class Crawler
  
  attr_reader :uploaded_pages
  
  def initialize( site_root, index, upload_dir )
    
    # Set instance variables
    @site_root = site_root
    @upload_dir = upload_dir
    
    # storage for uploaded pages
    # use page url as a key
    @uploaded_pages = {}
    
    # keep URL links to be uploaded as queue
    @upload_queue = [index]
    
    # init HTMLEntities object to decode URL links
    @coder = HTMLEntities.new
    
  end
  
  def start
    
    FileUtils.rm_rf( TMP_DIR ) if Dir.exist?( TMP_DIR ) # Remove old tmp
    
    # create tmp and upload dir
    Dir.mkdir( TMP_DIR ) 
    Dir.mkdir( UPLOAD_DIR ) unless Dir.exist?( UPLOAD_DIR )
    
    # crawl till link queue is empty
    while ! @upload_queue.empty? do
      crawl( @upload_queue.shift )
    end
    
  end
  
  private
  
  def crawl( url )
    
    # skip if page with given URL was allready uploaded 
    return if @uploaded_pages.include?( url )
    
    # if URL points to time shedule, change time period to long period ( change okres GET varialbe from 1 to 2 )
    if url.include?( "okres=1" )
      url = url.sub( "okres=1", "okres=2" )
    end
    
    # download and parse URL content
    page_content = download( url )
    
    # if url points to XML content
    if url.include? "xml"
      
      # set file extension
      file_extension = "xml"
      
      # parse xml content
      xml = parseXml( page_content )
      
      # Try to get timetable's list of types
      types = xml.xpath( "//grupowanie")
      
      # Try to get timetable's list of resources
      resources = xml.xpath( "//zasob")
      
      # check if xml is root xml file and contains list of timetable types
      if types.count > 0 
        
        # Than get list of uniq types of timetables
        uniq_types = [].tap { |a| types.each { |t| a << t.attr("typ") unless a.include? t.attr("typ") } }
        
        # Add each type to upload queue
        uniq_types.each { |t| @upload_queue << "index.php?typ=#{t}&xml" }
      
      end
    
      # check if xml contains list of timetables ( resources )
      if resources.count > 0 
        
        # Get type of timetables
        type = xml.xpath("/plan-zajec").attr("typ") 
        
        # Add timetable to upload queue
        resources.each do |r| 
          
          resource_id = r.attr( "id" )
          
          @upload_queue << "index.php?typ=#{type}&id=#{resource_id}&okres=2&xml" 
        
        end
        
      end
    
    # otherwise it is HTML content
    else
      
      file_extension = "html"
      
      # parse html content
      html = parseHtml( page_content )
      
      # get links from page
      links = ( url == "index.php" ) ? html.css( ".kategorie a") : html.css(".kolumny a")
      
      # add link to download queue
      links.each do |link|
        @upload_queue << link["href"] unless @upload_queue.include? link["href"]
      end
    
    end
    
    # init URI object to retrive query from decoded URL 
    uri = URI.parse( @coder.decode( url ) )
    
    # if no query set filename as "index"
    filename = "#{uri.query ? uri.query : "index"}.#{file_extension}"
    
    # save uploaded page 
    save( filename, html || xml )
    
    # Store uploaded page URI
    @uploaded_pages[url] = filename
    puts "\r#{@uploaded_pages.size}. #{url} uploaded"
    
    # wait a second to give crawled server a breath
    sleep SLEEP_TIME 
    
  end
  
  # download website content
  def download( url )
    RestClient.get( @site_root + url )
  end
  
  # parse website content
  def parseHtml( html )
    Nokogiri::HTML( html )
  end
  
  # parse xml content
  def parseXml( xml )
    Nokogiri::XML( xml )
  end
  
  # save file to upload dir and return full filename
  def save( filename, content )
    # write content to file
    File.open( "#{@upload_dir}/#{filename}" , 'w') { |file| file.write( content ) }
  end
  
end
    
crawler = Crawler.new( SITE_ROOT, "index.php?xml", UPLOAD_DIR )
crawler.start

json_index = JSON.pretty_generate( crawler.uploaded_pages )

# save uploaded files index 
File.open( TMP_DIR + "/upload_index.json" , 'w') { |file| file.write( json_index ) }
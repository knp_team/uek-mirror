<?php

// Set path to upload index
define( 'INDEX_PATH', './assets/upload_index.json' );
// Set dir with HTML pages
define( 'UPLOAD_DIR', './assets/upload/' );

// Read JSON index file
$index_json = file_get_contents( INDEX_PATH );
// Parse JSON
$index = json_decode( $index_json, true );

// Get requested URI and remove slash from it
$requested_uri = $_SERVER['REQUEST_URI']; strtr( $_SERVER['REQUEST_URI'], '/', '' ); 

// Add index.php to uri if uri points to site root
if ( $requested_uri == '' )
{
	$requested_uri = 'index.php';
}

// Delete slash from uri
$requested_uri = str_replace( '/', '', $requested_uri );
// Change $_GET duration variable if requested uri points to time schedule
$requested_uri = str_replace( 'okres=1', 'okres=2', $requested_uri );

// Get name of file with page contents
$page_filename = array_key_exists( $requested_uri, $index ) ? $index[$requested_uri] : null;

// Load requested page if page filename was found
$page = ( $page_filename ) ? file_get_contents( UPLOAD_DIR . $page_filename ) : 'Podana strona nie istnieje';

// Chagnge content type if page file has XML extension
if ( strpos( $page_filename, 'xml' ) !== false )
{
	header('Content-type: text/xml');
}

// Output page content
print $page;

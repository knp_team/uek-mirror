# UEK-Mirror 

UEK-Mirror is a web services that imitates a timetable web page of Cracow University of Economics. Mirrored timetable is available at [http://planzajec.uek.krakow.pl/](http://planzajec.uek.krakow.pl/).

### Running web service

If you wish to run UEK-Mirror just enter to the root dir of the repo and type in your console: 

```
bin/start.sh # Server will run on the port 8000 
```

Note that to run php build-in server __php  >= 5.4__ is required.

### Making timetable updates

In the near future the timetable might change. To fetch was changes run specially build ruby script that crawls the timetable website and download all pages. Before running script make sure you have all required games installed. If you are all set, run following commend in your console:

```
ruby src/crawler.rb 
```

The script will save crawled content `tmp` dir, so the last step is to swap contests of `assets` and `tmp` directories. Now you can start the web server once again.
